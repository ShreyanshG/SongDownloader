import wget
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

def get_download_link(driver):
	"""Error handled"""
	try :
		b = WebDriverWait(driver, 20).until(lambda driver : driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[4]/ul/li[1]/a'))
		b.click()
		w = WebDriverWait(driver, 20).until(lambda driver : driver.find_element_by_id('download-btn').get_attribute("href"))
		return w
	except:
		pass

def get_download_list(driver):
	
	SongList = [] #list to store download links
	for i in range(1, 6):
		b = WebDriverWait(driver, 20).until(lambda driver : driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[4]/ul/li['+str(i)+']/a'))#find download link
		SongName = WebDriverWait(driver, 20).until(lambda driver: driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[4]/ul/li['+str(i)+']/a/h3'))#find song name
		SongSize = WebDriverWait(driver, 20).until(lambda driver: driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[4]/ul/li['+str(i)+']/a/span'))#printing the size of the song
		Songartist = WebDriverWait(driver, 20).until(lambda driver: driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[4]/ul/li['+str(i)+']/a/p[1]'))#printing the name of the artist
		print "[" + str(i) + "] " + SongName.text, SongSize.text
		print Songartist.text
		SongList.append(b)#adds to list
	k = raw_input('Selection --> ')#song selection
	url = SongList[int(k)-1]#get selected url
	url.click()
	w = WebDriverWait(driver, 20).until(lambda driver : driver.find_element_by_id('download-btn').get_attribute("href"))#get download link
	return w

def main():
	a = raw_input('Song Name --> ').strip().split(" ")
	a = "+".join(a)
	url = 'https://musicpleer.audio/#!'+ a

	driver = webdriver.Firefox()
	driver.get(url)
	
	if '-l' in sys.argv[1:]:
		w = get_download_list(driver)
	else:
		w = w = get_download_link(driver)
	r = wget.download(w)
	driver.close()
	
if __name__=='__main__':
	main()
